package ru.alexp.tools.myrepo.repo;

import java.io.Serializable;

public class User implements Serializable {
    public final String login;
    public final String password;
    public final String[] passkey;

    public User(String login, String password, String[] passkey) {
        this.login = login;
        this.password = password;
        this.passkey = passkey;
    }
}
