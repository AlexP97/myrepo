package ru.alexp.tools.myrepo.repo.downloaders;

import ru.alexp.tools.myrepo.RFile;
import ru.alexp.tools.myrepo.repo.Repo;

import java.io.File;
import java.util.LinkedList;

public abstract class Downloader<R extends Repo> implements AutoCloseable {
    protected final LinkedList<DownloaderListener> listeners;
    protected R repo;
    protected RFile target;
    protected volatile State state;
    protected Thread thread;
    protected double progress = 0;
    protected boolean closed;
    private volatile boolean wait;

    public Downloader(R r) {
        listeners = new LinkedList<>();
        this.repo = r;
        thread = initThread();
        thread.setDaemon(true);
        setState(State.IDLE);
        closed = false;
    }

    public void setRFile(RFile file) throws DownloaderException {
        if (isState(State.DOWNLOADING) || isState(State.PAUSED))
            throw new DownloaderException("You can't set file while downloading.");
        this.target = file;
        setState(State.READY);
    }

    public RFile getFile() {
        return target;
    }

    public void download(File dir) throws DownloaderException {
        if (!isState(State.READY) || target == null)
            throw new DownloaderException("Target not found");
        setState(State.DOWNLOADING);
        listeners.forEach(DownloaderListener::onDownload);
        File local = new File(dir, getFile().getLocalFile().getPath());
        if (local.exists())
            if (!local.delete()) throw new DownloaderException("I can't delete file " + local.getAbsolutePath());
    }

    public void pause() throws DownloaderException {
        if (!isState(State.DOWNLOADING))
            throw new DownloaderException("Pausing not started downloader");
        setState(State.PAUSED);
        listeners.forEach(DownloaderListener::onPause);
        wait = true;
    }

    public void resume() throws DownloaderException {
        if (isState(State.IDLE) || target == null)
            throw new DownloaderException("Resuming stopped downloader");
        setState(State.DOWNLOADING);
        listeners.forEach(DownloaderListener::onResume);
        wait = false;
    }

    public void stop() throws DownloaderException {
        if (!(isState(State.DOWNLOADING) || isState(State.PAUSED)))
            throw new DownloaderException("Nothing to stop :)");
        setState(target == null ? State.IDLE : State.READY);
        listeners.forEach(DownloaderListener::onStop);
        wait = true;
    }

    public void close() {

    }

    public double getProgress() {
        return progress;
    }

    protected void setProcess(double process) {
        this.progress = process;
        listeners.forEach(l -> l.onProgress(process));
    }

    public void addListener(DownloaderListener l) {
        listeners.add(l);
    }

    protected abstract Thread initThread();

    public State getState() {
        return state;
    }

    protected void setState(State state) {
        this.state = state;
    }

    public boolean isState(State state) {
        return this.state.equals(state);
    }

    protected void runThread() {
        if (!isClosed()) {
            wait = false;
            try {
                thread.start();
            } catch (IllegalThreadStateException e) {
                // значит уже запущен
            }
        }
    }

    protected void waitTask() {
        while (wait) Thread.yield();
    }

    public boolean isClosed() {
        return closed;
    }

    public R getRepo() {
        return repo;
    }

    public boolean isPaused() {
        return getState().equals(State.PAUSED);
    }

    public boolean isStopped() {
        return isState(State.READY) || isState(State.IDLE);
    }

    public enum State {
        IDLE, READY, DOWNLOADING, PAUSED
    }
}
