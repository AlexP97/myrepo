package ru.alexp.tools.myrepo.repo.downloaders;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.alexp.tools.myrepo.RFile;
import ru.alexp.tools.myrepo.repo.Collection;

import java.io.File;

public class CollectionDownloader extends DownloaderListener.Adapter {

    private static final Logger log = LogManager.getLogger(CollectionDownloader.class);

    protected Downloader<?> downloader;
    protected Collection collection;

    protected long size = 0;
    protected long downloaded = 0;

    public CollectionDownloader() {
        this(null, null);
    }

    public CollectionDownloader(Downloader<?> downloader, Collection collection) {
        this.setDownloader(downloader);
        this.setCollection(collection);
    }

    public Downloader<?> getDownloader() {
        return downloader;
    }

    public void setDownloader(Downloader<?> downloader) {
        this.downloader = downloader;
        downloader.addListener(this);
    }

    public Collection getCollection() {
        return collection;
    }

    public void setCollection(Collection collection) {
        this.collection = collection;
        updateCollectionData();
    }

    public void download(File dir) throws DownloaderException, InterruptedException {
        for (RFile file : collection.getFiles()) {
            getDownloader().setRFile(file);
            getDownloader().download(dir);
            while (getDownloader().isState(Downloader.State.DOWNLOADING)) Thread.yield();
        }
    }

    public double getProgress() {
        return getDownloaded() / getSize();
    }

    public long getSize() {
        return size;
    }

    protected void setSize(long size) {
        this.size = size;
    }

    protected void updateCollectionData() {
        if (getCollection() != null) for (RFile f : getCollection().getFiles()) setSize(getSize() + f.getSize());
    }

    public long getDownloaded() {
        return downloaded;
    }

    protected void setDownloaded(long downloaded) {
        this.downloaded = downloaded;
    }

    @Override
    public void onProgress(double d) {
        //setDownloaded(gd); // TODO: 26.02.2016 допилить прогресс
    }
}
