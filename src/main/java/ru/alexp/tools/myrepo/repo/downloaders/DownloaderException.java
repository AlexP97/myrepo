package ru.alexp.tools.myrepo.repo.downloaders;

public class DownloaderException extends Exception {

    public DownloaderException(String message) {
        super(message);
    }

    public DownloaderException(String message, Throwable cause) {
        super(message, cause);
    }

    public DownloaderException(Throwable cause) {
        super(cause);
    }

}
