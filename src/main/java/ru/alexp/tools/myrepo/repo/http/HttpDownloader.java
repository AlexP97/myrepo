package ru.alexp.tools.myrepo.repo.http;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.alexp.tools.myrepo.Const;
import ru.alexp.tools.myrepo.repo.downloaders.Downloader;
import ru.alexp.tools.myrepo.repo.downloaders.DownloaderException;
import ru.alexp.tools.myrepo.util.HttpConnect;

import java.io.*;

public class HttpDownloader extends Downloader<HttpRepo> implements Runnable {

    private static final Logger log = LogManager.getLogger();

    private volatile HttpConnect conn;
    private volatile BufferedInputStream is;
    private volatile BufferedOutputStream os;

    public HttpDownloader(HttpRepo r) {
        super(r);
    }

    @Override
    public void download(File dir) throws DownloaderException {
        super.download(dir);
        if (log.isTraceEnabled())
            log.trace("Downloading " + getFile().getLocalFile().getPath() + " to " + dir.getPath());
        try {
            conn = getRepo().getConn();
            is = new BufferedInputStream(conn.getHttpURLConnection(
                    null, "GET", "?action=", "getFile", "&file=", getFile().getMd5().toString()).getInputStream());
            os = new BufferedOutputStream(
                    new FileOutputStream(new File(dir, getFile().getLocalFile().getPath())));
            runThread();
        } catch (Exception e) {
            throw new DownloaderException(e);
        }
    }

    @Override
    public void stop() throws DownloaderException {
        super.stop();
        try {
            is.close();
            os.close();
        } catch (Exception e) {
            throw new DownloaderException(e);
        }
    }

    @Override
    public Thread initThread() {
        return new Thread(this);
    }

    @Override
    public void run() {
        while (!isClosed()) {
            waitTask();
            byte[] buffer = new byte[Const.bufferSize];
            try {
                int size;
                int downloaded = 0;
                while ((size = is.read(buffer)) != -1) {
                    os.write(buffer, 0, size);
                    downloaded += size;
                    setProcess(downloaded / target.getSize());
                }
                stop();
            } catch (IOException | DownloaderException e) {
                log.warn(e);
            }
        }
    }
}
