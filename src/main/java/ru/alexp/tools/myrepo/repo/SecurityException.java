package ru.alexp.tools.myrepo.repo;

public class SecurityException extends RepoException {
    public SecurityException() {
        super();
    }

    public SecurityException(String message) {
        super(message);
    }

    public SecurityException(String message, Throwable cause) {
        super(message, cause);
    }
}
