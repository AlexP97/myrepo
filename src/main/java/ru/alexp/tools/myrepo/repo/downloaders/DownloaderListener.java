package ru.alexp.tools.myrepo.repo.downloaders;

public interface DownloaderListener {

    void onDownload();

    void onPause();

    void onResume();

    void onStop();

    void onProgress(double d);

    abstract class Adapter implements DownloaderListener {
        public void onDownload() {
        }

        public void onPause() {
        }

        public void onResume() {
        }

        public void onStop() {
        }

        public abstract void onProgress(double d);
    }
}
