package ru.alexp.tools.myrepo.repo;

import ru.alexp.tools.myrepo.RFile;

import java.util.ArrayList;

public class Collection {
    private final int id;
    private final String name;
    private final String descr;
    private final int createts;
    private final int editts;
    private final ArrayList<String> tags;
    private final ArrayList<RFile> files;

    public Collection(int id, String name, String descr, int createts,
                      int editts, ArrayList<String> tags, ArrayList<RFile> files) {
        this.id = id;
        this.name = name;
        this.descr = descr;
        this.createts = createts;
        this.editts = editts;
        this.tags = tags;
        this.files = files;
    }

    /**
     * Идентификатор коллекции
     */
    public int getId() {
        return id;
    }

    /**
     * Имя коллекции
     */
    public String getName() {
        return name;
    }

    /**
     * Описание коллекции (может быть null)
     */
    public String getDescr() {
        return descr;
    }

    /**
     * Дата (в UNIX формате) создания коллекции
     */
    public int getCreatets() {
        return createts;
    }

    /**
     * Дата (в UNIX формате) последнего изменения коллекции
     * (если коллекция создана недавно и нет изменений, то это поле равно предыдущему)
     */
    public int getEditts() {
        return editts;
    }

    /**
     * Теги коллекции через разделитель (может быть равно null)
     */
    public ArrayList<String> getTags() {
        return tags;
    }

    /**
     * Список id файлов включающихся в эту коллекцию
     */
    public ArrayList<RFile> getFiles() {
        return files;
    }

    /**
     * Возвращает true если в коллекции нет файлов
     */
    public boolean isEmpty() {
        return files.isEmpty();
    }

    /**
     * Возвращает true если в коллекции есть только один файл
     */
    public boolean isSingle() {
        return files.size() == 1;
    }

    /**
     * Возвращает true если в коллекции есть файл f
     */
    public boolean contains(RFile f) {
        return files.contains(f);
    }
}
