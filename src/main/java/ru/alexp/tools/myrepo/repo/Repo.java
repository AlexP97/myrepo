package ru.alexp.tools.myrepo.repo;

import ru.alexp.tools.myrepo.RFile;
import ru.alexp.tools.myrepo.repo.downloaders.Downloader;

import java.net.URI;
import java.util.LinkedList;

public abstract class Repo {
    protected final URI location;
    protected final Type type;

    protected final LinkedList<Collection> collections;

    public Repo(URI location, Type type) {
        this.location = location;
        this.type = type;
        this.collections = new LinkedList<>();
    }

    /**
     * Грузим список коллекций
     */
    public abstract void loadCollectionList() throws RepoException;

    public abstract Downloader getDownloader() throws RepoException;

    public boolean contains(Collection c) {
        return collections.contains(c);
    }

    public boolean conttains(RFile f) {
        for (Collection collection : collections)
            if (collection.contains(f))
                return true;
        return false;
    }

    public boolean isEmpty() {
        return collections.isEmpty();
    }

    public int size() {
        return collections.size();
    }

    public LinkedList<Collection> getCollections() {
        return collections;
    }

    /* при создании нового хранилища добавлять его тип сюда */
    public enum Type {
        HTTP
    }
}
