package ru.alexp.tools.myrepo.shell.commands;

import ru.alexp.tools.myrepo.Const;
import ru.alexp.tools.myrepo.crypt.AES128;
import ru.alexp.tools.myrepo.util.DoubleUtil;

import javax.crypto.CipherOutputStream;
import java.io.*;

public class EncryptFile extends Command {

    private static final int delay = 10; // sec

    @Override
    public String getName() {
        return "encodeFile";
    }

    @Override
    public String getDescr() {
        return "Encodes file.";
    }

    @Override
    public String[] getAliases() {
        return new String[]{"encode", "encrypt", "encryptFile"};
    }

    @Override
    public String getSyntax() {
        return "<file> <key> [output file]";
    }

    @Override
    public void onCommand(String... args) throws SyntaxException {
        if (args.length <= 1) throw new SyntaxException();
        final File target = new File(args[0]);
        if (!target.exists()) throw new SyntaxException("File " + target.getPath() + " not found.");
        if (!target.canRead()) throw new SyntaxException("Can't read file " + target.getPath() + ".");
        final File encPath;
        if (args.length >= 3) encPath = new File(args[2]);
        else
            encPath = new File(target.getParent(), "encoded-" + target.getName());
        try {
            final InputStream reader = new FileInputStream(target);
            final OutputStream writer = new FileOutputStream(encPath);

            final AES128 aes = new AES128(args[1].getBytes());
            final OutputStream aesWriter = new CipherOutputStream(writer, aes.getEncCipher());

            System.out.println("Encoding " + target.getPath() + " to " + encPath.getPath());

            copyWithProgress(reader, aesWriter, target.length());

            aesWriter.close();
            writer.close();
            reader.close();

            System.out.println("Encoded. See " + encPath.getPath());
        } catch (Exception e) {
            throw new SyntaxException(e.getMessage());
        }
    }

    private void copyWithProgress(InputStream reader, OutputStream writer, long length) throws IOException {
        long time = System.currentTimeMillis() / 1000;

        final byte[] buffer = new byte[Const.bufferSize];
        int loop = 0;
        final int loops = 100;
        long copied = 0;

        int n;
        while ((n = reader.read(buffer)) != -1) {
            writer.write(buffer, 0, n);
            copied += n;
            loop++;
            if (loop >= loops) {
                loop = 0;
                writer.flush();
            }
            if (System.currentTimeMillis() / 1000 - time >= delay) {
                time = System.currentTimeMillis() / 1000;
                System.out.println("Encoded " + DoubleUtil.round(((double) copied / length) * 100, 2) + "% (" + copied + "/" + length + " bytes)");
            }
        }
        writer.flush();
    }
}
