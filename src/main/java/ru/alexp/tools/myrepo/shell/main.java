package ru.alexp.tools.myrepo.shell;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.alexp.tools.myrepo.Const;
import ru.alexp.tools.myrepo.shell.commands.CommandManager;
import ru.alexp.tools.myrepo.shell.commands.DecryptFile;
import ru.alexp.tools.myrepo.shell.commands.EncryptFile;
import ru.alexp.tools.myrepo.shell.commands.Help;

import java.util.Arrays;

public class main {

    private static final Logger log = LogManager.getLogger();
    private static final CommandManager cm = new CommandManager();

    public static void main(String... args) throws Exception {
        registerCommands();
        printGreeting();

        if (args == null || args.length == 0)
            cm.invokeCommand("help");
        else if (args.length == 1)
            cm.invokeCommand(args[0]);
        else
            cm.invokeCommand(args[0], Arrays.copyOfRange(args, 1, args.length));
    }

    private static void printGreeting() {
        System.out.println("MyRepo client");
        System.out.println("Version " + Const.version);
        System.out.println();
    }

    private static void registerCommands() {
        cm.addCommand(new Help());
        cm.addCommand(new EncryptFile());
        cm.addCommand(new DecryptFile());
    }
}
