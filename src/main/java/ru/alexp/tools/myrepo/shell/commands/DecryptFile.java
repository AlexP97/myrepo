package ru.alexp.tools.myrepo.shell.commands;

import ru.alexp.tools.myrepo.Const;
import ru.alexp.tools.myrepo.crypt.AES128;
import ru.alexp.tools.myrepo.util.DoubleUtil;

import javax.crypto.CipherInputStream;
import java.io.*;

public class DecryptFile extends Command {

    private static final int delay = 10; // sec

    @Override
    public String getName() {
        return "decodeFile";
    }

    @Override
    public String getDescr() {
        return "Decodes file.";
    }

    @Override
    public String[] getAliases() {
        return new String[]{"decode", "decrypt", "decryptFile"};
    }

    @Override
    public String getSyntax() {
        return "<file> <key> [output file]";
    }

    @Override
    public void onCommand(String... args) throws SyntaxException {
        if (args.length <= 1) throw new SyntaxException();
        final File target = new File(args[0]);
        if (!target.exists()) throw new SyntaxException("File " + target.getPath() + " not found.");
        if (!target.canRead()) throw new SyntaxException("Can't read file " + target.getPath() + ".");
        final File decFile;
        if (args.length >= 3) decFile = new File(args[2]);
        else if (target.getName().startsWith("encoded-"))
            decFile = new File(target.getParent(), target.getName().substring("encoded-".length()));
        else
            decFile = new File(target.getParent(), "decoded-" + target.getName());
        try {
            final InputStream reader = new FileInputStream(target);
            final OutputStream writer = new FileOutputStream(decFile);

            final AES128 aes = new AES128(args[1].getBytes());
            final InputStream aesStream = new CipherInputStream(reader, aes.getDecCipher());

            System.out.println("Decoding " + target.getPath() + " to " + decFile.getPath());

            copyWithProgress(aesStream, writer, target.length());

            aesStream.close();
            writer.close();
            reader.close();

            System.out.println("Decoded. See " + decFile.getPath());
        } catch (Exception e) {
            throw new SyntaxException(e.getMessage());
        }
    }

    private void copyWithProgress(InputStream reader, OutputStream writer, long length) throws IOException {
        long time = System.currentTimeMillis() / 1000;

        final byte[] buffer = new byte[Const.bufferSize];
        int loop = 0;
        final int loops = 100;
        long copied = 0;

        int n;
        while ((n = reader.read(buffer)) != -1) {
            writer.write(buffer, 0, n);
            copied += n;
            loop++;
            if (loop >= loops) {
                loop = 0;
                writer.flush();
            }
            if (System.currentTimeMillis() / 1000 - time >= delay) {
                time = System.currentTimeMillis() / 1000;
                System.out.println("Decoded " + DoubleUtil.round(((double) copied / length) * 100, 2) + "% (" + copied + "/" + length + " bytes)");
            }
        }
        writer.flush();
    }
}
