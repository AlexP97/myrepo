package ru.alexp.tools.myrepo;

public final class Const {

    public static final String version = "0.0.0-ALPHA";
    public static final String appName = "MyRepo";

    public static final int bufferSize = 1024 * 2;
}
