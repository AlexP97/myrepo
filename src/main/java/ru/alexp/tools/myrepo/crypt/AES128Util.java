package ru.alexp.tools.myrepo.crypt;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.util.Random;

public class AES128Util {

    private static final Logger log = LogManager.getLogger(AES128Util.class);
    private static AES128 aes;

    public static SecretKeySpec loadKeyFromFile(File f) throws IOException, ClassNotFoundException {
        CipherInputStream cipherIS = new CipherInputStream(new FileInputStream(f), getBaseCryptor().getDecCipher());
        BufferedInputStream fileIS = new BufferedInputStream(cipherIS);
        ObjectInputStream ois = new ObjectInputStream(fileIS);
        Object o = ois.readObject();
        ois.close();
        fileIS.close();
        return (SecretKeySpec) o;
    }

    public static AES128 getAES128FromFile(File f) {
        try {
            SecretKeySpec key = loadKeyFromFile(f);
            return new AES128(key);
        } catch (IOException | ClassNotFoundException e) {
            return null;
        }
    }

    public static void saveAES128KeyToFile(File f, SecretKeySpec key) throws IOException {
        CipherOutputStream cipherOS = new CipherOutputStream(new FileOutputStream(f), getBaseCryptor().getEncCipher());
        BufferedOutputStream fileOS = new BufferedOutputStream(cipherOS);
        ObjectOutputStream oos = new ObjectOutputStream(fileOS);
        oos.writeObject(key);
        oos.close();
        fileOS.close();
    }

    public static void saveAES128KeyToFile(File f, AES128 aes) {
        try {
            saveAES128KeyToFile(f, aes.getKeySpec());
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    public static AES128 getBaseCryptor() {
        if (aes == null) aes = new AES128();
        return aes;
    }

    public static byte[] getRandomBytes(int length) {
        final byte[] bytes = new byte[length];
        final Random rnd = new Random();
        rnd.nextBytes(bytes);
        return bytes;
    }

    public static AES128 getRandomAES128Cryptor() {
        return new AES128(getRandomBytes(AES128.keyLength));
    }
}
