package ru.alexp.tools.myrepo.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import ru.alexp.tools.myrepo.RFile;
import ru.alexp.tools.myrepo.md5;

import java.io.File;
import java.util.ArrayList;

public final class JSONUtil {

    private static final Logger log = LogManager.getLogger(JSONUtil.class);

    public static ArrayList<String> toStringArrayList(JSONArray a) {
        final ArrayList<String> result = new ArrayList<>();
        for (int i = 0; i < a.length(); i++)
            result.add(a.getString(i));
        return result;
    }

    public static ArrayList<RFile> parseRFilesArrayList(JSONArray array) {
        final ArrayList<RFile> result = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            RFile file = RFile.empty;
            try {
                JSONObject rfile = array.getJSONObject(i);
                if (rfile.has("md5") && rfile.has("path")) {
                    file = new RFile(new md5(rfile.getString("md5")),
                            new File(rfile.getString("path")));
                    file.setSize(rfile.optInt("size"));
                }
            } catch (Exception t) {
                log.warn(t);
            }
            if (file != RFile.empty) result.add(file);
        }
        return result;
    }
}
