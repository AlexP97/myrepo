<?php

/**
 * Класс в котором хранятся все конфигурации сайта.
 * Пример: config::getConfig('main', 'siteName');
 *
 * © Alex_P
 */
class config
{
    private static $config = array(
        'main' => array(
            'isDebug' => true,
            'isRepoClosed' => false,
            'repoName' => 'GR Host',
            'fileDir' => '/file/'
        ),
        'base' => array(
            'host' => 'localhost',
            'port' => '3306',
            'user' => 'root',
            'pass' => 'pass'
        ),
        'mail' => array(
            'host' => 'ssl://smtp.yandex.ru',
            'port' => 465,
            'login' => 'login@ya.ru',
            'pass' => 'pass',
            'name' => 'PostBot Name',
        )
    );

    public static function getConfig($key, $param)
    {
        return config::$config[$key][$param];
    }
}