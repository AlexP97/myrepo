<?php

/**
 * Класс sql предназначен для создания
 * соединений с базой данных.
 */
class sql
{

    private static $PDOconnection = null;

    /* Start of PDO */

    public static function createPDOConnection($databaseName = null)
    {
        sql::$PDOconnection = new PDO(
            'mysql:host=' . config::getConfig('base', 'host') . ':' . config::getConfig('base', 'port') . ';dbname=' . config::getConfig('base', 'defaultDatabase'),
            config::getConfig('base', 'user'),
            config::getConfig('base', 'pass')
        );
        if (!sql::$PDOconnection) {
            echo "PDOerrorCreation:\n";
            print_r(sql::$PDOconnection->errorInfo());
        }
    }

    public static function getPDOConnection($databaseName = null, $isGetNewConnection = false)
    {
        if ($isGetNewConnection || sql::$PDOconnection == null) {
            sql::createPDOConnection($databaseName);
        }
        return sql::$PDOconnection;
    }

    public static function PDOQuery($query, $databaseName = null)
    {
        $query = sql::getPDOConnection($databaseName)->query($query);

        if (!$query) {
            echo "PDOerror:\n";
            print_r(sql::$PDOconnection->errorInfo());
        } else {
            return $query;
        }
    }

    public static function quotePDO($s, $databaseName = null)
    {
        return sql::getPDOConnection($databaseName)->quote($s);
    }

    public static function closePDO()
    {
        sql::$PDOconnection = null;
    }

    /* End of PDO */

    public static function closeAllConnections()
    {
        sql::closePDO();
    }
}

?>