<?php

class mail
{
    private static $mailer;

    public static function createMailer()
    {
        mail::$mailer = new SendMailSmtp(config::getConfig('mail', 'login'), config::getConfig('mail', 'pass'), config::getConfig('mail', 'host'), config::getConfig('mail', 'login'), config::getConfig('mail', 'port'));
    }

    public static function getMailer()
    {
        if (mail::$mailer == null)
            mail::createMailer();

        return mail::$mailer;
    }

    public static function send($mail, $text, $subject = '')
    {
        $header = "MIME-Version: 1.0\r\n" .
            "Content-type: text/html; charset=utf-8\r\n" .
            "From: " . config::getConfig('mail', 'name') . " <" . config::getConfig('mail', 'login') . ">\r\n";

        return mail::getMailer()->send($mail, $subject, $text, $header);
    }
}