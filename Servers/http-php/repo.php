<?php

/**
 * Alex_P repo engine.
 * Version 0.0.0-ALPHA.
 *
 * Все конфигурации находятся
 * в ./php/config.php
 */

$phpPath = dirname(__FILE__) . '/php/';

require $phpPath . 'init.php';
require $phpPath . 'config.php';
require $phpPath . 'sql.php';
require $phpPath . 'SendMailSmtp.php';
require $phpPath . 'mail.php';

registerGlobals();
loadLoggerSettings();

session_start();

if (isset($_GET['action'])) {
    $file = $phpPath . 'modules/' . $_GET['action'] . '.php';
    if (file_exists($file)) {
        require $file;
        echo json_encode(getResponse());
    }
}